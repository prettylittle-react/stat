import React from 'react';
import PropTypes from 'prop-types';

import './Stat.scss';

/**
 * Stat
 * @description [Description]
 * @example
  <div id="Stat"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Stat, {
        title : 'Example Stat'
    }), document.getElementById("Stat"));
  </script>
 */
class Stat extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'stat';
	}

	render() {
		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				Stat
			</div>
		);
	}
}

Stat.defaultProps = {
	children: null
};

Stat.propTypes = {
	children: PropTypes.node
};

export default Stat;
